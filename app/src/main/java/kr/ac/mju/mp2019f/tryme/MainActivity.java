package kr.ac.mju.mp2019f.tryme;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private View bgView;
    private Button button;
    private int[] colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colors = new int[] {Color.RED, Color.BLUE, Color.CYAN, Color.GREEN, Color.GRAY, Color.WHITE, Color.YELLOW};

        bgView = findViewById(R.id.bgViewID);
        button = findViewById(R.id.buttonID);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rnd = new Random();
                int num = rnd.nextInt(colors.length);
                Log.d("Random", String.valueOf(num));

                bgView.setBackgroundColor( colors[num]);
            }
        });

    }
}
